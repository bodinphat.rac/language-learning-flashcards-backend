import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { PlayService } from './play.service';
import { GetUser } from '../auth/decorator';
import { JwtGuard } from 'src/auth/guard';
import { sendDeckDto } from './dto/sendDeckDto';

@Controller('play')
export class PlayController {
  constructor(private playService: PlayService) {}

  @UseGuards(JwtGuard)
  @Get('card/:id')
  getCard(
    @GetUser('id') userId: number,
    @Param('id') deckidStr: string,
    @Query('first') first: string,
  ) {
    return this.playService.getCard(userId, deckidStr, first);
  }

  @UseGuards(JwtGuard)
  @Post('send/:id/:level')
  sendCard(
    @GetUser('id') userId: number,
    @Param('id') cardidStr: string,
    @Param('level') level: string,
  ) {
    return this.playService.sendCard(userId, cardidStr, level);
  }

  @UseGuards(JwtGuard)
  @Get('starter/card/:id')
  startergetCard(
    @GetUser('id') userId: number,
    @Param('id') deckidStr: string,
    @Query('first') first: string,
  ) {
    return this.playService.startergetCard(userId, deckidStr, first);
  }

  @UseGuards(JwtGuard)
  @Post('starter/send/:id/:level')
  startersendCard(
    @GetUser('id') userId: number,
    @Param('id') cardidStr: string,
    @Param('level') level: string,
  ) {
    return this.playService.startersendCard(userId, cardidStr, level);
  }

  @UseGuards(JwtGuard)
  @Get('deck/:id')
  getDeck(@GetUser('id') userId: number, @Param('id') deckidStr: string) {
    return this.playService.getDeck(userId, deckidStr);
  }
  @UseGuards(JwtGuard)
  @Post('deck/:id')
  sendDeck(
    @GetUser('id') userId: number,
    @Param('id') deckidStr: string,
    @Body() dto: sendDeckDto,
  ) {
    return this.playService.sendDeck(userId, deckidStr, dto);
  }
  @UseGuards(JwtGuard)
  @Get('starter/deck/:id')
  getstarterDeck(
    @GetUser('id') userId: number,
    @Param('id') deckidStr: string,
  ) {
    return this.playService.getstarterDeck(userId, deckidStr);
  }
  @UseGuards(JwtGuard)
  @Post('starter/deck/:id')
  sendstarterDeck(
    @GetUser('id') userId: number,
    @Param('id') deckidStr: string,
    @Body() dto: sendDeckDto,
  ) {
    return this.playService.sendstarterDeck(userId, deckidStr, dto);
  }
}
