import { IsString, IsNotEmpty } from 'class-validator';
export class CreateCardDto {
  @IsString()
  @IsNotEmpty()
  word: string;

  @IsString()
  @IsNotEmpty()
  pronunciationLanguage: string;

  @IsString()
  @IsNotEmpty()
  pronunciationEng: string;

  @IsString()
  @IsNotEmpty()
  pronunciationThai: string;

  @IsString()
  @IsNotEmpty()
  translation: string;

  state: number;
}
