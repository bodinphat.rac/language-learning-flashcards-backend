import { IsString, IsOptional } from 'class-validator';
export class UpdateCardDto {
  @IsString()
  @IsOptional()
  word?: string;

  @IsString()
  @IsOptional()
  pronunciationLanguage?: string;

  @IsString()
  @IsOptional()
  pronunciationEng?: string;

  @IsString()
  @IsOptional()
  pronunciationThai?: string;

  @IsString()
  @IsOptional()
  translation?: string;

  state: number;
}
