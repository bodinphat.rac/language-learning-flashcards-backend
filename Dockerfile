FROM alpine:latest

RUN apk add yarn nodejs

RUN mkdir /opt/llf-backend
COPY . /opt/llf-backend

WORKDIR /opt/llf-backend
RUN yarn
RUN yarn add @prisma/client
CMD yarn run start
