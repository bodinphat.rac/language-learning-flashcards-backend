import { Type } from 'class-transformer';
import {
  IsString,
  IsOptional,
  IsArray,
  // ArrayNotEmpty,
  ValidateNested,
  IsNumber,
} from 'class-validator';
class Card {
  @IsNumber()
  @IsOptional()
  id?: number;

  @IsString()
  @IsOptional()
  word?: string;

  @IsString()
  @IsOptional()
  pronunciationLanguage?: string;

  @IsString()
  @IsOptional()
  pronunciationEng?: string;

  @IsString()
  @IsOptional()
  pronunciationThai?: string;

  @IsString()
  @IsOptional()
  translation?: string;

  state: number;
}

export class UpdateDeckDto {
  @IsString()
  @IsOptional()
  nameInLanguage?: string;

  @IsString()
  @IsOptional()
  nameEng?: string;

  @IsString()
  @IsOptional()
  nameThai?: string;

  @IsArray()
  //@ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => Card)
  cards: Card[];
}
