import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { SignInDto, SignUpDto } from './dto';
import * as argon from 'argon2';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwt: JwtService,
    private config: ConfigService,
  ) {}
  async signup(dto: SignUpDto) {
    // generate the password hash
    const password = await argon.hash(dto.password);
    // save the new user to db
    try {
      const user = await this.prisma.user.create({
        data: {
          username: dto.username,
          nickname: dto.nickname,
          totalExp: 0,
          loginSteak: 1,
          profileImage: '',
          password,
        },
      });
      //set last login to midnight of tomorrow
      const midnight_tomorrow = new Date();
      midnight_tomorrow.setDate(midnight_tomorrow.getDate() + 1);
      midnight_tomorrow.setHours(0, 0, 0, 0);

      await this.prisma.user.update({
        where: { username: dto.username },
        data: { lastLogin: midnight_tomorrow },
      });
      //return token
      return this.signToken(user.id, user.username);
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new ForbiddenException('Credentials taken');
        }
      }
      throw error;
    }
  }

  async signin(dto: SignInDto) {
    //find the user by email
    const user = await this.prisma.user.findFirst({
      where: {
        username: dto.username,
      },
    });

    //if user doesn't exist throw exception
    if (!user) throw new ForbiddenException('Credentials incorrect');

    //compare password
    const pwMatches = await argon.verify(user.password, dto.password);

    //if password incorrect throw exception
    if (!pwMatches) throw new ForbiddenException('Credentials incorrect');
    if (new Date().getTime() - user.lastLogin.getTime() >= 86400000) {
      await this.prisma.user.update({
        where: { username: dto.username },
        data: { loginSteak: user.loginSteak + 1 },
      });
    }
    //set last login to midnight of tomorrow
    const midnight_tomorrow = new Date();
    midnight_tomorrow.setDate(midnight_tomorrow.getDate() + 1);
    midnight_tomorrow.setHours(0, 0, 0, 0);

    await this.prisma.user.update({
      where: { username: dto.username },
      data: { lastLogin: midnight_tomorrow },
    });
    //return token
    return this.signToken(user.id, user.username);
  }

  async signToken(
    userId: number,
    username: string,
  ): Promise<{ access_token: string }> {
    const payload = {
      sub: userId,
      username,
    };
    const secret = this.config.get('JWT_SECRET');

    const token = await this.jwt.signAsync(payload, {
      expiresIn: '1d',
      secret: secret,
    });

    return {
      access_token: token,
    };
  }
}
