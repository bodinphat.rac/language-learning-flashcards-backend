-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,
    "lastLogin" TIMESTAMP(3) NOT NULL,
    "loginSteak" INTEGER NOT NULL,
    "totalExp" INTEGER NOT NULL,
    "username" TEXT NOT NULL,
    "nickname" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "profileImage" TEXT NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Deck" (
    "id" SERIAL NOT NULL,
    "nameInLanguage" TEXT NOT NULL,
    "nameEng" TEXT NOT NULL,
    "nameThai" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,
    "userId" INTEGER NOT NULL,
    "experince" INTEGER NOT NULL,
    "maxExperince" INTEGER NOT NULL,
    "playCount" INTEGER NOT NULL,
    "totalEasy" INTEGER NOT NULL,
    "totalHard" INTEGER NOT NULL,
    "totalSkip" INTEGER NOT NULL,
    "easy" INTEGER NOT NULL,
    "hard" INTEGER NOT NULL,
    "skip" INTEGER NOT NULL,
    "nowIdx" INTEGER NOT NULL,
    "maxIdx" INTEGER NOT NULL,

    CONSTRAINT "Deck_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Card" (
    "id" SERIAL NOT NULL,
    "word" TEXT NOT NULL,
    "pronunciationLanguage" TEXT NOT NULL,
    "pronunciationEng" TEXT NOT NULL,
    "pronunciationThai" TEXT NOT NULL,
    "translation" TEXT NOT NULL,
    "state" INTEGER NOT NULL,
    "selected" INTEGER NOT NULL,
    "deckId" INTEGER NOT NULL,

    CONSTRAINT "Card_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "StarterDeck" (
    "id" SERIAL NOT NULL,
    "nameInLanguage" TEXT NOT NULL,
    "nameEng" TEXT NOT NULL,
    "nameThai" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "StarterDeck_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "StarterCard" (
    "id" SERIAL NOT NULL,
    "word" TEXT NOT NULL,
    "pronunciationLanguage" TEXT NOT NULL,
    "pronunciationEng" TEXT NOT NULL,
    "pronunciationThai" TEXT NOT NULL,
    "translation" TEXT NOT NULL,
    "deckId" INTEGER NOT NULL,

    CONSTRAINT "StarterCard_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserAndStarterDeckRelation" (
    "id" SERIAL NOT NULL,
    "userId" INTEGER NOT NULL,
    "deckId" INTEGER NOT NULL,
    "experince" INTEGER NOT NULL,
    "maxExperince" INTEGER NOT NULL,
    "playCount" INTEGER NOT NULL,
    "totalEasy" INTEGER NOT NULL,
    "totalHard" INTEGER NOT NULL,
    "totalSkip" INTEGER NOT NULL,
    "easy" INTEGER NOT NULL,
    "hard" INTEGER NOT NULL,
    "skip" INTEGER NOT NULL,
    "nowIdx" INTEGER NOT NULL,
    "maxIdx" INTEGER NOT NULL,

    CONSTRAINT "UserAndStarterDeckRelation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserAndStarterCardRelation" (
    "id" SERIAL NOT NULL,
    "userId" INTEGER NOT NULL,
    "fromDeckId" INTEGER NOT NULL,
    "cardId" INTEGER NOT NULL,
    "state" INTEGER NOT NULL,
    "selected" INTEGER NOT NULL,

    CONSTRAINT "UserAndStarterCardRelation_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");

-- AddForeignKey
ALTER TABLE "Deck" ADD CONSTRAINT "Deck_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Card" ADD CONSTRAINT "Card_deckId_fkey" FOREIGN KEY ("deckId") REFERENCES "Deck"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "StarterCard" ADD CONSTRAINT "StarterCard_deckId_fkey" FOREIGN KEY ("deckId") REFERENCES "StarterDeck"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
